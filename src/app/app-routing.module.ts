import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ExcelOperationsComponent } from './Pages/excel-operations/excel-operations.component';

const routes: Routes = [
  {path:'', component:ExcelOperationsComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
